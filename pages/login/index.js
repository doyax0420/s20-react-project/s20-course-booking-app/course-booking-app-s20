import { Row, Col } from 'react-bootstrap';
import View from '../../components/View';
import LoginForm from '../../components/LoginForm';

export default function index() {
return(
	<View title={'Log In'}>
		<Row className="justify-content-center">
			<Col xs md="6">
				<h3>Log In</h3>
				<LoginForm/>
			</Col>
		</Row>
	</View>
)
}
import React , { useState, useEffect } from 'react';
import { Container, Form, Button } from 'react-bootstrap';
import Head from 'next/head';

export default function index(){
	const [email, setEmail] = useState('')
	const [password1, setPassword1] = useState('')
	const [password2, setPassword2] = useState('')
	//state to determine wether submit button is enabled or not
	const [isActive, setIsActive] = useState(false)

	function registerUser(e){
		e.preventDefault()

		console.log(`${email} is registered with password ${password1}`)

		setEmail('')
		setPassword1('')
		setPassword2('')
		alert("thank you for registering")
	}

	useEffect(() => {
		if((email !== '' && password1 !== '' && password2 !== '') && (password1 === password2)){
			setIsActive(true)
		}else{
			setIsActive(false)
		}
	}, [email, password1, password2])

	return(
        <React.Fragment>
            <Head>
                <title>Next-Booking Registration</title>
            </Head>
            <Container>
                <Form onSubmit={e => registerUser(e)}>
                    <Form.Group controlId="userEmail">
                        <Form.Label>Email Address</Form.Label>
                        <Form.Control type="email" placeholder="Enter email" value={email} onChange={e => setEmail(e.target.value)} required/>
                        <Form.Text className="text-muted">We'll never share your email with anyone else</Form.Text>
                    </Form.Group>

                    <Form.Group controlId="password1">
                        <Form.Label></Form.Label>
                        <Form.Control type="password" placeholder="Enter password" value={password1} onChange={e => setPassword1(e.target.value)} required/>
                    </Form.Group>

                    <Form.Group controlId="password">
                        <Form.Label></Form.Label>
                        <Form.Control type="password" placeholder="Verify password" value={password2} onChange={e => setPassword2(e.target.value)} required/>
                    </Form.Group>

                    {/*Conditionally render submit button based on isActive state*/}
                    {isActive
                        ? <Button variant="primary" type="submit" id="submitBtn">Submit</Button>

                        : <Button variant="primary" type="submit" id="submitBtn" disabled>Submit</Button>
                    }

                </Form>
            </Container>
        </React.Fragment>
	)

}
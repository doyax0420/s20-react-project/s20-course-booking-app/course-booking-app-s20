//create a logout page that will do the following:
// 1. Consume UserContext and obtain unsetUser and setUser from it
// 2. invoke unsetUser()
// 3. Using setUser, set the user state objects email property to null
// 4. redirect back to login <- DONE
// 5. implement the logout page in our app
import UserContext from '../../UserContext'
import { useEffect, useContext } from 'react';
import Router from 'next/router';

export default function index(){

    const { setUser, unsetUser } = useContext(UserContext)
 
    useEffect(() => {
        unsetUser();
        Router.push('/login')
    }, [])
    return null
}
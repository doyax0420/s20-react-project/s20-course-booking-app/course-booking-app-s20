import React, { useContext } from 'react';
import Course from '../../components/Course';
//import coursesData from '../../data/courses';
import { Container, Table, Button } from 'react-bootstrap'
import Head from 'next/head';
import AppHelper from '../../app-helper';
import UserContext from '../../UserContext';

export async function getServerSideProps(){
	const res = await fetch(`${ AppHelper.API_URL}/courses`)
	const coursesData = await res.json()

	return { props: {coursesData} }
}

export default function index({ coursesData }){

	//consume UserContext and destructure it to acces global user state
	const { user } = useContext(UserContext)

	//create multiple course components corresponding to the number of courses in out coursesData
	const courses = coursesData.map(courseData => {
			return(
				//pass data to the course component via props
				<Course key={courseData._id} course={courseData}/>
			)	
	})

	//table rows to be rendered when admin is logged in
	const coursesRows = coursesData.map(courseData => {
		return(
			<tr>
				<td>{courseData._id}</td>
				<td>{courseData.name}</td>
				<td>PHP {courseData.price}</td>
				<td>{courseData.onOffer ? 'Open' : 'Closed'}</td>
				<td>{courseData.start_date}</td>
				<td>{courseData.end_date}</td>
				<td>
					<Button variant="warning">Update</Button>
					<Button variant="danger">Disable</Button>
				</td>
			</tr>
		)
	})
    
	return(
        
		user.isAdmin === true
        ?
        <React.Fragment>
            <Head>
            <title>Next Booking Admin Panel</title>
        </Head>
        <Container>
			<h1>Course Dashboard</h1>
			<Table striped bordered hover>
				<thead>
					<tr>
						<th>ID</th>
						<th>Name</th>
						<th>Price</th>
						<th>Status</th>
						<th>Start Date</th>
						<th>End Date</th>
						<th>Actions</th>
					</tr>
				</thead>
				<tbody>
					{coursesRows}
				</tbody>
			</Table>
		</Container>
        </React.Fragment>		
        :
        <React.Fragment>
            <Head>
            <title>Next Booking Courses</title>
            </Head>
            <Container>
                {courses}
            </Container>
        </React.Fragment>         
	)
}
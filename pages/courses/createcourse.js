//create a page for adding new course that uses a form with the ff parameters

//courseName
//description
//price
//startDate
//endDate

//add a function in this page that prevents default form behavior, then logs in the 
//console a message that shows that course is created by showing its name, startdate
//and price. The function then clears/resets all inputs

//in navbar.js, conditionally render a nav item poiting to /course/create that will 
//render the create course page when an admin is logged in, and then fully integrate
//the create course page to our project

import React, { useState, useEffect } from 'react';
import { Container, Form, Button } from 'react-bootstrap';
import Head from 'next/head';

export default function CreateCourse(){
    const [courseName, setCourseName] = useState('');
    const [description, setDescription] = useState('');
    const [price, setPrice] = useState('');
    const [startDate, setStartDate] = useState('');
    const [endDate, setEndDate] = useState('');
    
    function addCourse(e){

        e.preventDefault()
        
        console.log(`Course ${courseName} created with start date ${startDate} and price PHP ${price}`)
        console.log(`Desc: ${description}`)
        setCourseName('');
        setDescription('');
        setPrice('');
        setStartDate('');
        setEndDate('');
    }

    useEffect(() => {
        if(courseName !== '' && description !== '' && price !== '' && startDate !=='' && endDate !== ''){
            console.log("USEEFFECT")
            console.log(`Course name: ${courseName}`)
            console.log(`Description: ${description}`)
            console.log(`Price: PHP ${price}`)
            console.log(`Start date: ${startDate}`)
            console.log(`End date: ${endDate}`)
        }
    }, [courseName, description, price, startDate, endDate])

    return(
    <React.Fragment>
        <Head>
            <title>Next-Booking Creat Course</title>
        </Head>
        <Container>
            <Form onSubmit={e => addCourse(e)}>
                <Form.Group controlId="courseName">
                    <Form.Label>Name</Form.Label>
                    <Form.Control type="text" placeholder="Enter name" value={courseName} onChange={e => setCourseName(e.target.value)} required/>
                </Form.Group>

                <Form.Group controlId="description">
                    <Form.Label>Description</Form.Label>
                    <Form.Control type="text" placeholder="Enter description" value={description} onChange={e => setDescription(e.target.value)} required/>
                </Form.Group>

                <Form.Group controlId="price">
                    <Form.Label>Price</Form.Label>
                    <Form.Control type="text" placeholder="Enter price" value={price} onChange={e => setPrice(e.target.value)} required/>
                </Form.Group>

                <Form.Group controlId="startDate">
                    <Form.Label>Start Date</Form.Label>
                    <Form.Control type="text" placeholder="Enter start date" value={startDate} onChange={e => setStartDate(e.target.value)} required/>
                </Form.Group>

                <Form.Group controlId="endDate">
                    <Form.Label>Price</Form.Label>
                    <Form.Control type="text" placeholder="Enter end date" value={endDate} onChange={e => setEndDate(e.target.value)} required/>
                </Form.Group>
                <Button type="submit" variant="primary">Add</Button>
            </Form>
        </Container>
    </React.Fragment>        
    )
}
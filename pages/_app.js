import { useState, useEffect } from 'react'
import '../styles/globals.css'
import 'bootstrap/dist/css/bootstrap.min.css';
import NavBar from '../components/NavBar';
import { UserProvider } from '../UserContext';
import AppHelper from '../app-helper';

function MyApp({ Component, pageProps }) {
  const [user , setUser] = useState({
    id: null,
    isAdmin: null
  })

  useEffect(() => {
    const options = {
      headers: { Authorization: `Bearer ${AppHelper.getAccessToken() }`}
    }
    fetch(`${ AppHelper.API_URL}/users/details`, options)
    .then(AppHelper.toJSON)
    .then(data => {
      if(typeof data._id !== 'undefined'){
        setUser({
          id: data._id,
          isAdmin: data.isAdmin
        })
      }else{
        setUser({
          id: null,
          isAdmin: null
        })
      }
    })
  },[user.id])

  const unsetUser = () => {
    localStorage.clear()
    setUser({
      id: null,
      isAdmin: null
    })
  }
  return(
    <UserProvider value={{user, setUser, unsetUser}}>
      <NavBar />
      <Component {...pageProps} />
    </UserProvider>
    
  )
}

export default MyApp

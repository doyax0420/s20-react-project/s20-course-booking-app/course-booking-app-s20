import React from 'react';
import Head from 'next/head'
import Banner from '../components/Banner'
import Highlights from '../components/Highlights'
import {Container} from 'react-bootstrap';

export default function Home() {
  const pageData = {
    title: "Zuitt Coding Bootcamp",
		content: "Opportunities for everyone, everywhere",
		destination: "/courses",
		label: "Enroll now!"
  }
  return (
    <React.Fragment>
      <Head>
         <title>Next-Booking Home Page</title>
      </Head>
      <Banner data={pageData}/>
      <Container>
      <Highlights />
      </Container>
    </React.Fragment>
  )
}

import { useState, useEffect } from 'react' //import the useState hook
import { Card, Button } from 'react-bootstrap'
import PropTypes from 'prop-types'
import AppHelper from '../app-helper';

//Create a card component showing a particular course with the following information:
//Name in Card Title
//Description in Card Text
//Price in Card Text
//Button with "Enroll" as its label

//once created display the component in our Home.js page

export default function Course({course}) {
	const {name, description, price, start_date, end_date } = course
	// const [count, setCount] = useState(0)
	// const [seats, setSeats] = useState(10)
	// const [isOpen, setIsOpen] = useState(true)

	//Create another state hook on the Course component representing the number of available seats for the course. Each course will have 30 available seats by default. Show this on the component.

	// useEffect(() => {
	// 	if(seats === 0){
	// 		setIsOpen(false)
	// 	}
	// }, [seats])// [count, seats] the value or values in the array is an optional parameter (but there must be always an array, even if blank) React will re-run useEffect ONLY if any of the values in this array change

	function enroll(courseId){		
		fetch(`${AppHelper.API_URL}/users/enroll`,{
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${ AppHelper.getAccessToken() }`
			},
			body: JSON.stringify({
				courseId: courseId
			})
		})
		.then(AppHelper.toJSON)
		.then(data => {
			console.log(data)
		})		
	}

	return(
		<Card>
		  <Card.Body>
		    <Card.Title>{name}</Card.Title>
		    <Card.Text>
		      <span className="subtitle">Description:</span>
			  <br />
			  {description}
			  <br />
			  <span className="subtitle">Price: </span>
			  PHP {price}
			  <br />
			  <span className="subtitle">Start Date: </span>
			  {start_date}
			  <br />
			  <span className="subtitle">End Date: </span>
			  {end_date}
			  <br />
		    </Card.Text>
		    	<Button variant="primary" onClick={() => enroll(course._id)}>Enroll</Button>	    
		  </Card.Body>
		</Card>
	)
}


//check that the course component is getting the correct prop types
Course.propTypes = {
	//shape() is used to check the prop object conforms to a specific format
	course: PropTypes.shape({
		name: PropTypes.string.isRequired,
		description: PropTypes.string.isRequired,
		price: PropTypes.number.isRequired,
		start_date: PropTypes.string.isRequired,
		end_date: PropTypes.string.isRequired
	})
}